FROM hashicorp/vault:1.13.1 as vault
FROM alpine:latest

COPY --from=vault /bin/vault usr/local/bin/

WORKDIR /build
ARG UID=1000
ARG GID=1000
ENV USER=mantis

RUN apk add --update nodejs npm curl jq unzip
RUN npm install -g @bitwarden/cli 

RUN curl -LO https://releases.hashicorp.com/packer/1.8.6/packer_1.8.6_linux_amd64.zip 
RUN unzip ./packer_1.8.6_linux_amd64 -d packer
RUN chmod +x ./packer/packer
RUN cp ./packer/packer /usr/local/bin/

RUN curl -LO https://releases.hashicorp.com/terraform/1.4.2/terraform_1.4.2_linux_amd64.zip
RUN unzip ./terraform_1.4.2_linux_amd64.zip -d terraform
RUN chmod +x ./terraform/terraform
RUN cp ./terraform/terraform /usr/local/bin/


RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl


# install sudo as root
# RUN apk add --update sudo

# add new user
# RUN adduser -D $USER \
#         && echo "$USER ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/$USER \
#         && chmod 0440 /etc/sudoers.d/$USER
RUN addgroup -g "${GID}"  ci
RUN adduser --disabled-password -h /home/mantis  -u "${UID}" -G "ci" mantis
ENV HOME=/home/$USER
# COPY ["./data/","/home/mantis/.config/Bitwarden CLI/"]
# files in /home/$USER to be owned by $USER
# docker has --chown flag for COPY, but it does not expand ENV so we fallback to:
RUN chown -R $UID:$GID $HOME
RUN chown -R $UID:$GID /usr/local/bin
# COPY src src
USER $USER
WORKDIR $HOME


